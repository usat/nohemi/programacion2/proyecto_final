let correo = localStorage.getItem("email")
document.getElementById("myproduct").style.display = "none"

if (!correo || correo == "undefined") {
	document.getElementById("block-usuario").style.display = "none"
	document.getElementById("block-cerrar").style.display = "none"
	document.getElementById("block-login").style.display = "block"
} else {
	document.getElementById("block-usuario").style.display = "block"
	document.getElementById("block-cerrar").style.display = "block"
	document.getElementById("block-login").style.display = "none"

	document.getElementById("txtblockusuario").innerText = localStorage.getItem(
		"email"
	)

	if (localStorage.getItem("acceso") === "empresa") {
		document.getElementById("myproduct").style.display = "block"
	}
}

document.getElementById("btncerrar").onclick = () => {
	localStorage.clear()
	location.reload()
}

document.getElementById("myproduct").onclick = () => {
	if (localStorage.getItem("acceso") === "empresa") {
		window.location = `myproduct.html`
	}
}

let tabla = document.getElementById("cards-container")

function listar() {
	$.ajax({
		type: "POST",
		url: "../service/producto/listar.php",
		data: "",
		success: function(response) {
			//console.log(response)
			//return
			let data = JSON.parse(response)

			llenar_listar(data)
		}
	})
}

document.getElementById("directory-search").onclick = () => {
	window.location = `productos.html?id=${
		document.getElementById("js-input-what").value
	}`
}

function llenar_listar(data) {
	let html = ""

	for (let i = 0; i < data.length; i++) {
		html += `<div class="col-xs-12 col-sm-4">`
		html += `<div class="card card--directory" data-live-scroll="443090">`
		html += `<div class="js-card-photo-container card__photo-container">`
		html += `<div class="swiper-container js-card-swiper" data-media-url="https://www.zankyou.com.pe/f/salon-de-recepciones-el-golf-443090/card/get-wervice-slider-images.json" data-media-start="2">`
		html += `<div class="swiper-wrapper" data-href="https://www.zankyou.com.pe/f/salon-de-recepciones-el-golf-443090">`
		html += `<div class="swiper-slide card__photo--directory js-media-slide" data-url="https://asset3.zankyou.com/images/wervice-card/49c/b57c/640/424/w/443090/-/1459441278.jpg">`
		html += `<img class="card__photo-img lazyload js-lazyload" alt="Salón de Recepciones El Golf" src="../service/fotos/${
			data[i].foto
		}" style="display: block;width: 200px;margin: 0 20%;">	</div>`
		html += `<div class="swiper-slide card__photo--directory js-media-slide" data-url="https://asset2.zankyou.com/images/wervice-card/d1a/a8b4/640/424/w/443090/-/1459442096.jpg">`
		html += `<img class="card__photo-img swiper-lazy" alt="Salón de Recepciones El Golf" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://asset2.zankyou.com/images/wervice-card/d1a/a8b4/640/424/w/443090/-/1459442096.jpg"></div>`
		html += `<div class="swiper-slide card__photo--directory js-media-slide" data-url="https://asset2.zankyou.com/images/wervice-card/512/fb30/640/424/w/443090/-/1459442083.jpg">`
		html += `<img class="card__photo-img swiper-lazy" alt="Salón de Recepciones El Golf" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://asset2.zankyou.com/images/wervice-card/512/fb30/640/424/w/443090/-/1459442083.jpg"></div>`
		html += `</div>`
		html += `<div class="slider__arrow slider__arrow--big slider__arrow--prev slider__arrow--clean js-arrow-prev swiper-button-disabled">`
		html += `<span class="icon icon_arrow-left" title="Anterior"></span>`
		html += `</div>`
		html += `<div class="slider__arrow slider__arrow--big slider__arrow--next slider__arrow--clean js-arrow-next">`
		html += `<span class="icon icon_arrow-right" title="Siguiente"></span>`
		html += `</div>`
		html += `</div>`
		html += `<div class="card__fav">`
		html += `<button data-selection="saved" data-wervice-id="443090" data-status="" data-origin="card" class="u-button--reset">`
		html += `<i class="u-icon--animate icon icon_heart-full js-active js-selection-saved hidden"></i>`
		html += `</button>`
		html += `</div>`
		html += `</div>`
		html += `<div class="card__data">`
		html += `<div class="card__content">`
		html += `<a href="detalle-producto.html?id=${data[i].id_producto}">`
		html += `<span class="card__name">`
		html += `${data[i].empresa}`
		html += `</a>`
		html += `<div class="card__cat">`
		html += `${data[i].categoria} - ${data[i].tipo_categoria}	</div>`
		html += `</div>`
		html += `</div>`
		html += `<div class="card__actions js-card__actions">`
		html += `<div class="row">`
		html += `<div class="col-xs-12">`
		html += `<a class="zk-button--square--black u-link--reset" href='detalle-producto.html?id=${
			data[i].id_producto
		}'">`
		html += `<i class="u-color--green"></i>`
		html += `<i class="u-color--green hidden"></i>`
		html += `<span class="button-text hidden">lr a la conversación</span>`
		html += `<span class="button-text">`
		html += `Contactar	</span>`
		html += `</a>`
		html += `</div>`
		html += `</div>`
		html += `</div>`
		html += `</div>`
		html += `</div>`
	}

	tabla.innerHTML = html
}

listar()
