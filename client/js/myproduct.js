let tabla = document.getElementById("tabla")
let modal_title = document.getElementById("modal_titulo")
let accion = document.getElementById("accion").value

function listar() {
	$.ajax({
		type: "POST",
		url: "../producto/leer2.php",
		data: { email: localStorage.getItem("email") },
		success: function(response) {
			let data = JSON.parse(response)
			llenar_tabla(data)
		}
	})
}

function llenar_tabla(data) {
	let html = ""

	html += `<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">`
	html += `<thead>`
	html += `<tr>`
	html += `<th>Empresa</th>`
	html += `<th>CATEGORIA</th>`
	html += `<th>TIPO CATEGORIA</th>`
	html += `<th>NOMBRE</th>`
	// html += `<th>FOTO</th>`
	html += `<th class="text-center">ACCIONES</th>`
	html += `</tr>`
	html += `</thead>`
	html += `<tbody>`

	for (let i = 0; i < data.length; i++) {
		html += `<tr>`
		html += `<td>${data[i].empresa}</td>`
		html += `<td>${data[i].categoria}</td>`
		html += `<td>${data[i].tipo_categoria}</td>`
		html += `<td>${data[i].nombre}</td>`
		// html += `<td>${data[i].foto}</td>`
		html += `<td nowrap class="text-center">`
		// html += `<button type="button" class="btn btn-warning" onclick="editar('${data[i].email}','${data[i].id_tipo_categoria}')" style="margin-right: 5px"><i class="flaticon-edit" style="padding-right: 0"></i></button>`
		html += `<button type="button" class="btn btn-danger" onclick="eliminar('${
			data[i].id_producto
		}')" style="margin-right: 5px"><i class="flaticon-close" style="padding-right: 0"></i></button>`
		html += `</td>`
		html += `</tr>`
	}
	html += `</tbody>`
	html += `</table>`

	tabla.innerHTML = html

	var table = $("#kt_table_1")
	table.DataTable({
		responsive: true,
		paging: true
	})
}

function llenar_combo_tipo_categoria() {
	$.ajax({
		type: "POST",
		url: "../tipo_categoria/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			$("#modal_combo_tipo_categoria").html(response)
		}
	})
}

function eliminar(id_producto) {
	Swal.fire({
		title: "¿Desea eliminar este registro?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Si, Eliminar"
	}).then(result => {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "../producto/eliminar.php",
				data: { id_producto },
				success: function(response) {
					if (response == 0) {
						Swal.fire("Elimado!", "El registro ha sido eliminado.", "success")

						listar()
					}
				}
			})
		}
	})
}

$("#formulario").submit(function(e) {
	e.preventDefault()

	let email = localStorage.getItem("email")
	let id_tipo_categoria = document.getElementById("modal_id_tipo_categoria")
		.value
	let nombre = document.getElementById("modal_nombre").value
	let descripcion = document.getElementById("modal_descripcion").value
	// var foto = $("#foto").prop("files")

	console.log(email, id_tipo_categoria, nombre, descripcion, foto)

	var datos = new FormData()

	datos.append("email", email)
	datos.append("id_tipo_categoria", id_tipo_categoria)
	datos.append("nombre", nombre)
	datos.append("descripcion", descripcion)
	// datos.append("foto", foto)
	var ins = document.getElementById("foto").files.length
	for (var x = 0; x < ins; x++) {
		console.log(document.getElementById("foto").files[x])
		datos.append("foto[]", document.getElementById("foto").files[x])
	}

	for (var pair of datos.entries()) {
		console.log(pair[0] + ", " + pair[1])
	}

	if (accion == 0) {
		$.ajax({
			type: "POST",
			url: "../producto/agregar.php",
			data: datos,
			enctype: "multipart/form-data",
			processData: false, // tell jQuery not to process the data
			contentType: false,
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Agregado!", "El registro ha sido agregado.", "success")

					listar()
				}
			}
		})
	} else {
		// $.ajax({
		// 	type: "POST",
		// 	url: "../distrito/editar.php",
		// 	data: { id_distrito: id_distrito, id_departamento: id_departamento, id_provincia: id_provincia, nombre: nombre },
		// 	success: function (response) {
		// 		if (response == 0) {
		// 			$('#modal').modal('toggle');
		// 			Swal.fire(
		// 				'Editado!',
		// 				'El registro ha sido editado.',
		// 				'success'
		// 			)
		// 			listar()
		// 		}
		// 	}
		// })
	}
})

// function editar(id_departamento, id_provincia, id_distrito) {
// 	$('#modal').modal('toggle');

// 	modal_title.innerHTML = "Editar"
// 	accion = 1

// 	llenar_combo_provincia(id_departamento)

// 	$.ajax({
// 		type: "POST",
// 		url: "../distrito/leer.php",
// 		data: { id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito },
// 		success: function (response) {

// 			let data = JSON.parse(response)
// 			console.log(data.id_provincia)

// 			$("#modal_id").val(data.id_distrito)

// 			$("#modal_id_departamento").val(data.id_departamento)
// 			$("#modal_id_provincia").val(data.id_provincia)

// 			$("#modal_nombre").val(data.nombre)

// 		}
// 	})
// }

function agregar() {
	accion = 0
	$("#modal_id").val("")
	$("#modal_nombre").val("")
	$("#modal_id_departamento").prop("selectedIndex", 0)
	$("#modal_combo_provincia").html("")
	modal_title.innerHTML = "Agregar"
}

listar()
llenar_combo_tipo_categoria()
