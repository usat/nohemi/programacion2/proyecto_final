let correo = localStorage.getItem("email")
if (!correo || correo == "undefined") {
	alert("por favor, incie sesión")
	window.location = `login.html`

	document.getElementById("block-usuario").style.display = "none"
	document.getElementById("block-cerrar").style.display = "none"
	document.getElementById("block-login").style.display = "block"
} else {
	document.getElementById("block-usuario").style.display = "block"
	document.getElementById("block-cerrar").style.display = "block"
	document.getElementById("block-login").style.display = "none"

	document.getElementById("txtblockusuario").innerText = localStorage.getItem(
		"email"
	)
}

document.getElementById("btncerrar").onclick = () => {
	localStorage.clear()
	location.reload()
}

let id_producto = window.location.search
	.slice(1)
	.split("&")[0]
	.split("=")[1]

function listar() {
	$.ajax({
		type: "POST",
		url: "../service/producto/leer.php",
		data: { id_producto },
		success: function(response) {
			let data = JSON.parse(response)
			// console.log(data)
			llenar_listar(data)
		}
	})
}

function email() {
	let resultado = ""
	$.ajax({
		type: "POST",
		url: "../service/producto/email.php",
		async: false,
		data: { id_producto },
		success: function(response) {
			resultado = JSON.parse(response)
		}
	})

	return resultado
}

function llenar_listar(data) {
	// console.log(data)

	if (data.length == 0) {
		window.location = `index.html`
	}

	document.getElementById("title-card").innerText = data[0].nombre
	document.getElementById("page-descripcion").innerText = data[0].descripcion
	document.getElementById("subtitle-card").innerText = `${
		data[0].categoria
	} - ${data[0].tipo_categoria}`

	let images = new Array()

	data.map((v, i) => {
		let obj = {
			src: v.foto,
			srct: v.foto,
			title: v.tipo_categoria
		}

		images.push(obj)
	})

	console.log(images)

	jQuery("#nanogallery2").nanogallery2({
		// ### gallery settings ###
		thumbnailHeight: 150,
		thumbnailWidth: 150,
		itemsBaseURL: "http://localhost/proyecto_final-master/service/fotos/",

		// ### gallery content ###
		items: images
	})
}

document.getElementById("btn-contactar").onclick = () => {
	let data = email()

	Email.send({
		SecureToken: "7f84f6f6-a8ea-474a-b724-205169dfe529",
		To: localStorage.getItem("email"),
		From: "programacionusat@gmail.com",
		Subject: "Información",
		Body: template_email(data)
	}).then(message => {
		//enviando los datos al servidor y si responde OK entonces enviamos el dato al servidor para guardarlo en la bd
		alert(message)
	})
}

listar()
