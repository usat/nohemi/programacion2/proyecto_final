function template_email(data) {
	let html = ``
	html += `	<body style="margin: 0; padding: 0;" cz-shortcut-listen="true">`
	html += `		<table border="0" cellpadding="0" cellspacing="0" width="100%">`
	html += `			<tbody>`
	html += `				<tr>`
	html += `					<td style="padding: 10px 0 30px 0;">`
	html += `						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">`
	html += `							<tbody>`
	html += `								<tr>`
	html += `									<td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">`
	html += `										<img src="https://i.imgur.com/jDK67ry.png" alt="Creating Email Magic" width="300" height="230" style="display: block;">`
	html += `									</td>`
	html += `								</tr>`
	for (let i = 0; i < data.length; i++) {
		html += `								<tr>`
		html += `									<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">`
		html += `										<table border="0" cellpadding="0" cellspacing="0" width="100%">`
		html += `											<tbody>`
		html += `												<tr>`
		html += `													<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">`
		html += `														<b>${data[i].empresa}</b>`
		html += `													</td>`
		html += `												</tr>`
		html += `												<tr>`
		html += `													<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">`
		html += `														<p>Telefono: ${data[i].telf1}</p>`
		html += `														<p>Telefono: ${data[i].telf2}</p>`
		html += `														<p>Email: ${data[i].email}</p>`
		html += `														<p>Dirección: ${data[i].direccion} -  ${data[i].departamento}/ ${
			data[i].provincia
		}/ ${data[i].distrito}</p>`
		html += `													</td>`
		html += `												</tr>`
		html += `												<tr>`
		html += `													<td>`
		html += `														<table border="0" cellpadding="0" cellspacing="0" width="100%">`
		html += `															<tbody>`
		html += `																<tr>`
		html += `																	<td width="260" valign="top">`
		html += `																		<table border="0" cellpadding="0" cellspacing="0" width="100%">`
		html += `																			<tbody>`
		html += `																				<tr>`
		html += `																					<td style="`
		html += `    text-align: -webkit-center;`
		html += `">`
		html += `																						<img src="https://i.imgur.com/jDK67ry.png" alt="" width="auto" height="140" style="display: block;">`
		html += `																					</td>`
		html += `																				</tr>`
		html += `																				<tr>`
		html += `																					<td style="padding: 25px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">`
		html += `																						<p>Producto: ${data[i].nombre}</p>`
		html += `																						<p>Categoria - Subcategoria: ${data[i].categoria}-${
			data[i].tipo_categoria
		}</p>`
		html += `																						<p>Descripcion: ${data[i].descripcion}</p>`
		html += `																					</td>`
		html += `																				</tr>`
		html += `																			</tbody>`
		html += `																		</table>`
		html += `																	</td>`
		html += `																</tr>`
		html += `															</tbody>`
		html += `														</table>`
		html += `													</td>`
		html += `												</tr>`
		html += `											</tbody>`
		html += `										</table>`
		html += `									</td>`
		html += `								</tr>`
	}
	html += `								<tr>`
	html += `									<td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">`
	html += `										<table border="0" cellpadding="0" cellspacing="0" width="100%">`
	html += `											<tbody>`
	html += `												<tr>`
	html += `													<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">`
	html += `														® Programación 2019`
	html += `													</td>`
	html += `												</tr>`
	html += `											</tbody>`
	html += `										</table>`
	html += `									</td>`
	html += `								</tr>`
	html += `							</tbody>`
	html += `						</table>`
	html += `					</td>`
	html += `				</tr>`
	html += `			</tbody>`
	html += `		</table>`
	html += `	`
	html += ``
	html += `</body>`
	html += ``
	return html
}
