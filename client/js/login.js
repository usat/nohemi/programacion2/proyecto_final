function hasClass(ele, cls) {
	return ele.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"))
}

function removeClass(ele, cls) {
	if (hasClass(ele, cls)) {
		var reg = new RegExp("(\\s|^)" + cls + "(\\s|$)")
		ele.className = ele.className.replace(reg, " ")
	}
}

document.getElementById("kt_login_signin_submit").onclick = () => {
	// alert("hola")
	// debugger
	let email = document.getElementById("txtemail").value
	let password = document.getElementById("txtpassword").value

	$.ajax({
		type: "POST",
		url: "../usuario/login.php",
		data: { email, password },
		success: function(response) {
			console.log(response)
			if (response) {
				// debugger
				// console.log(response)
				let data = JSON.parse(response)
				localStorage.setItem("id_usuario", data.id_usuario)
				localStorage.setItem("email", data.email)
				localStorage.setItem("id_acceso", data.id_acceso)
				localStorage.setItem("acceso", data.acceso)

				let correo = localStorage.getItem("email")
				if (!correo || correo === "undefined") {
					alert("datos incorrectos")
					location.reload()
				} else {
					window.location = `../../client/index.html`
				}
			}
		}
	})
}

document.getElementById("kt_login_signup_submit").onclick = () => {
	if (document.getElementById("tipo").value == 2) {
		let apellido_paterno = document.getElementById("cli-apellido-paterno").value
		let apellido_materno = document.getElementById("cli-apellido-materno").value
		let nombres = document.getElementById("cli-nombres").value
		let telefono1 = document.getElementById("cli-telefono1").value
		let telefono2 = document.getElementById("cli-telefono2").value
		let direccion = document.getElementById("cli-direccion").value
		let id_departamento = document.getElementById("modal_id_departamento").value
		let id_provincia = document.getElementById("modal_id_provincia").value
		let id_distrito = document.getElementById("modal_id_distrito").value
		let email = document.getElementById("cli-email").value
		let pass1 = document.getElementById("cli-pass1").value
		let pass2 = document.getElementById("cli-pass2").value

		if (pass1 != pass2) {
			alert("contraseñas diferentes")
			return
		}

		$.ajax({
			type: "POST",
			url: "../cliente/agregar.php",
			data: {
				email: email,
				apellido_paterno: apellido_paterno,
				apellido_materno: apellido_materno,
				nombres: nombres,
				// sexo: sexo,
				direccion: direccion,
				// fecha_nacimiento: fecha_nacimiento
				// 	.split("/")
				// 	.reverse()
				// 	.join("-"),
				telf1: telefono1,
				telf2: telefono2,
				id_departamento: id_departamento,
				id_provincia: id_provincia,
				id_distrito: id_distrito
			},
			success: function(response) {
				if (response == 0) {
					alert("se guardo")
					// simulateClick(document.getElementById("kt_login_signup_cancel"))
					location.reload()
				}
			}
		})

		$.ajax({
			type: "POST",
			url: "../usuario/agregar.php",
			data: {
				email,
				password: pass1,
				id_acceso: document.getElementById("tipo").value
			},
			success: function(response) {
				if (response == 0) {
				}
			}
		})
	} else {
		let nombre = document.getElementById("emp-nombre").value
		let rubro = document.getElementById("emp-rubro").value
		let telefono1 = document.getElementById("emp-telefono1").value
		let telefono2 = document.getElementById("emp-telefono2").value
		let direccion = document.getElementById("emp-direccion").value
		let id_departamento = document.getElementById("modal_id_departamento").value
		let id_provincia = document.getElementById("modal_id_provincia").value
		let id_distrito = document.getElementById("modal_id_distrito").value
		let email = document.getElementById("emp-email").value
		let pass1 = document.getElementById("emp-pass1").value
		let pass2 = document.getElementById("emp-pass2").value

		if (pass1 != pass2) {
			alert("contraseñas diferentes")
			return
		}

		$.ajax({
			type: "POST",
			url: "../empresa/agregar.php",
			data: {
				email: email,
				nombre: nombre,
				rubro: rubro,
				direccion: direccion,
				telf1: telefono1,
				telf2: telefono2,
				id_distrito: id_distrito,
				id_departamento: id_departamento,
				id_provincia: id_provincia
			},
			success: function(response) {
				if (response == 0) {
					alert("se guardo")
					// simulateClick(document.getElementById("kt_login_signup_cancel"))
					location.reload()
				}
			}
		})

		$.ajax({
			type: "POST",
			url: "../usuario/agregar.php",
			data: {
				email,
				password: pass1,
				id_acceso: document.getElementById("tipo").value
			},
			success: function(response) {
				if (response == 0) {
				}
			}
		})
	}
}

document.getElementById("kt_login_forgot_submit").onclick = () => {
	let email = document.getElementById("kt_email").value
	let password = aleatorio()

	let resultado = ""
	$.ajax({
		type: "POST",
		url: "../usuario/find-email.php",
		async: false,
		data: { email },
		success: function(response) {
			if (response) {
				resultado = JSON.parse(response)
			}
		}
	})

	if (email === resultado.email) {
		$.ajax({
			type: "POST",
			url: "../usuario/restore-password.php",
			async: false,
			data: { email, password },
			success: function(response) {
				console.log(response)
			}
		})

		Email.send({
			SecureToken: "7f84f6f6-a8ea-474a-b724-205169dfe529",
			To: email,
			From: "programacionusat@gmail.com",
			Subject: "Recuperar contraseña",
			Body: `Su nueva contraseña es ${password}`
		}).then(message => {
			//enviando los datos al servidor y si responde OK entonces enviamos el dato al servidor para guardarlo en la bd
			if (message == "OK") {
				simulateClick(document.getElementById("kt_login_forgot_cancel"))
			}
		})
	}
}

function aleatorio() {
	let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	let lon = 32
	let code = ""

	for (let x = 0; x < lon; x++) {
		rand = Math.floor(Math.random() * chars.length)
		code += chars.substr(rand, 1)
	}

	return code
}

var simulateClick = function(elem) {
	// Create our event (with options)
	var evt = new MouseEvent("click", {
		bubbles: true,
		cancelable: true,
		view: window
	})
	// If cancelled, don't dispatch our event
	var canceled = !elem.dispatchEvent(evt)
}

$("#tipo").on("change", function() {
	llenar_combo_departamento()

	if (this.value == 3) {
		document.getElementById("sign-empresa").style.display = "block"
		document.getElementById("sign-cliente").style.display = "none"
	} else {
		document.getElementById("sign-empresa").style.display = "none"
		document.getElementById("sign-cliente").style.display = "block"
	}
})

function llenar_combo_departamento() {
	$.ajax({
		type: "POST",
		url: "../departamento/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			if (document.getElementById("tipo").value == 2) {
				$("#cli-modal_combo_departamento").html(response)
				$("#emp-modal_combo_departamento").html("")
			} else {
				$("#cli-modal_combo_departamento").html("")
				$("#emp-modal_combo_departamento").html(response)
			}
		}
	})
}

function llenar_combo_provincia(id_departamento) {
	$.ajax({
		type: "POST",
		url: "../provincia/combo.php",
		data: { id_departamento: id_departamento },
		success: function(response) {
			// console.log(response)
			if (document.getElementById("tipo").value == 2) {
				$("#cli-modal_combo_provincia").html(response)
				$("#emp-modal_combo_provincia").html("")
			} else {
				$("#cli-modal_combo_provincia").html("")
				$("#emp-modal_combo_provincia").html(response)
			}
		}
	})
}

function llenar_combo_distrito(id_departamento, id_provincia) {
	$.ajax({
		type: "POST",
		url: "../distrito/combo.php",
		data: { id_departamento: id_departamento, id_provincia: id_provincia },
		success: function(response) {
			// console.log(response)
			if (document.getElementById("tipo").value == 2) {
				$("#cli-modal_combo_distrito").html(response)
				$("#emp-modal_combo_distrito").html("")
			} else {
				$("#cli-modal_combo_distrito").html("")
				$("#emp-modal_combo_distrito").html(response)
			}
		}
	})
}

$(document).on("change", "#modal_id_departamento", function() {
	let id_departamento = $("#modal_id_departamento").val()

	$("#cli-modal_combo_provincia").html("")
	$("#cli-modal_combo_distrito").html("")

	llenar_combo_provincia(id_departamento)
})

$(document).on("change", "#modal_id_provincia", function() {
	let id_departamento = $("#modal_id_departamento").val()
	let id_provincia = $("#modal_id_provincia").val()

	$("#cli-modal_combo_distrito").html("")

	llenar_combo_distrito(id_departamento, id_provincia)
})

llenar_combo_departamento()
