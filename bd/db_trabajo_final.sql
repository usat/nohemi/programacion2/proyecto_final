-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jun 21, 2019 at 06:01 AM
-- Server version: 8.0.16
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_trabajo_final`
--

-- --------------------------------------------------------

--
-- Table structure for table `acceso`
--

CREATE TABLE `acceso` (
  `id_acceso` int(11) NOT NULL,
  `nombre` varbinary(100) NOT NULL
);

--
-- Dumping data for table `acceso`
--

INSERT INTO `acceso` (`id_acceso`, `nombre`) VALUES
(2, 0x636c69656e7465),
(3, 0x656d7072657361),
(4, 0x61646d696e6973747261646f72);

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL
);

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`) VALUES
(11, 'Local'),
(12, 'Musica');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `email` varchar(100) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `telf1` varchar(50) DEFAULT NULL,
  `telf2` varchar(50) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_departamento` char(2) DEFAULT NULL,
  `id_provincia` char(2) DEFAULT NULL,
  `id_distrito` char(2) DEFAULT NULL
);

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`email`, `apellido_paterno`, `apellido_materno`, `nombres`, `telf1`, `telf2`, `direccion`, `id_departamento`, `id_provincia`, `id_distrito`) VALUES
('asdassdfa@gmail.com', 'adsf', '', 'kjhkjl', 'kljhklj', 'kljhkjl', 'kljhkj', '02', '01', '01'),
('final7@gmail.com', 'final', 'final2', 'final3', 'final4', 'final5', 'final6', '14', '01', '01'),
('prueba@gmail.com', 'paterno', '', 'nombres', 'telef1', 'telef2', 'direccion', '14', '01', '01'),
('qweeqw@gmail.com', 'asdf', '', 'asdf', 'kjhlkj', 'jkhjk', 'jkhjk', '02', '01', '01'),
('sadfdfa@gmail.com', '123', '234', '876', '7896', '9876', '8976', '02', '01', '01'),
('silviopd01@gmail.com', 'asdf', 'asdwerqwe', 'jkhjk', 'kjhlkj', 'jkhjk', 'jkhjk', '02', '01', '01');

-- --------------------------------------------------------

--
-- Table structure for table `contacto`
--

CREATE TABLE `contacto` (
  `id_contacto` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `email_cliente` varchar(100) DEFAULT NULL,
  `id_producto` int(10) UNSIGNED DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `id_departamento` char(2) NOT NULL,
  `nombre` varchar(200) NOT NULL
);

--
-- Dumping data for table `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `nombre`) VALUES
('02', 'La Libertad'),
('14', 'Lambayeque');

-- --------------------------------------------------------

--
-- Table structure for table `distrito`
--

CREATE TABLE `distrito` (
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `id_distrito` char(2) NOT NULL,
  `nombre` varchar(200) NOT NULL
);

--
-- Dumping data for table `distrito`
--

INSERT INTO `distrito` (`id_departamento`, `id_provincia`, `id_distrito`, `nombre`) VALUES
('02', '01', '01', 'chicama'),
('14', '01', '01', 'Chiclayo'),
('14', '01', '02', 'JLO'),
('14', '02', '01', 'asdhjk'),
('14', '02', '02', 'wiqw');

-- --------------------------------------------------------

--
-- Table structure for table `empresa`
--

CREATE TABLE `empresa` (
  `email` varchar(100) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `rubro` varchar(50) DEFAULT NULL,
  `direccion` varchar(200) NOT NULL,
  `telf1` varchar(50) DEFAULT NULL,
  `telf2` varchar(50) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_departamento` char(2) DEFAULT NULL,
  `id_provincia` char(2) DEFAULT NULL,
  `id_distrito` char(2) DEFAULT NULL
);

--
-- Dumping data for table `empresa`
--

INSERT INTO `empresa` (`email`, `nombre`, `rubro`, `direccion`, `telf1`, `telf2`, `id_departamento`, `id_provincia`, `id_distrito`) VALUES
('autor6@gmail.com', 'autor1', 'autor2', 'autor5', 'autor3', 'autor4', '02', '01', '01'),
('empresa1@gmail.com', 'Empresa Musical', 'Musica', 'Av. Bolognesi 149', '074-123524', '9798361034', '14', '01', '01'),
('pazvilcheznoemipaola@gmail.com', 'Nohemi', 'Alquiler', 'Av. Miraflores 324', '074-234523', '9798231034', '14', '01', '01');

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE `foto` (
  `id_producto` int(10) UNSIGNED NOT NULL,
  `n_foto` int(10) UNSIGNED NOT NULL,
  `foto` varchar(100) DEFAULT NULL
);

--
-- Dumping data for table `foto`
--

INSERT INTO `foto` (`id_producto`, `n_foto`, `foto`) VALUES
(1, 1, '1506964775546_-375036469.JPG'),
(1, 2, '1506964776263_1478831071.JPG'),
(1, 3, '1506964743355_-378730553.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `email` varchar(100) NOT NULL,
  `id_tipo_categoria` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_producto` int(10) UNSIGNED NOT NULL,
  `descripcion` mediumtext
);

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`email`, `id_tipo_categoria`, `nombre`, `id_producto`, `descripcion`) VALUES
('pazvilcheznoemipaola@gmail.com', 5, 'Local Comercial', 1, ' Sobre la Carretera a Pimentel y a pocos metros del Òvalo Evitamiento ( Autopista del Sol). Intenso desarrollo inmobiliario en la zona. Existen 6 universidades en un radio de 02 Kilòmetros. La oficina principal se encuentra en Lima con amplios y modernos depósitos en Lurín. \n\n03 Frentes, de los cuales el principal tiene 225 ml. hacia la carretera a Pimentel. \n\n05 Almacenes completamente independientes. Amplios Patios de Maniobra Internos Iluminados. \n\nDISTRIBUCION: 05 Almacenes con diversas áreas y 01 oficina independiente en 2do Piso con 336 m2 y cocheras. \n\nSecciòn 1: Almacén / Área techada: 1,391 mts2 Alquiler : US$ Precio del anuncio +IGV \nSecciòn 2: Almacén / Àrea techada: 633 mts2 ARRENDADO \nSecciòn 3: Almacén / Area Techada: 4043 mts2 Alquiler : US$ 17,183 + IGV \nSección 4: Almacén / Area Techada: 559 mts2 Alquiler : US$ 2,516 + IGV \nSección 5: Almacén / Area Techada: 853 mts2 Alquiler : US$ 3,412 + IGV \n\nSección 6: Oficinas Ejecutivas + 8 cocheras / Area Techada: 336 mts2 Alquiler : US$2,688 + IGV \n\nAmplias Vías interiores acceden a todos los almacenes (Ver fotos). Precio Negociable de acuerdo a forma de pago ó aumento en el espacio de Alquiler. ');

-- --------------------------------------------------------

--
-- Table structure for table `provincia`
--

CREATE TABLE `provincia` (
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `nombre` varchar(200) NOT NULL
);

--
-- Dumping data for table `provincia`
--

INSERT INTO `provincia` (`id_departamento`, `id_provincia`, `nombre`) VALUES
('02', '01', 'Trujillo'),
('14', '01', 'Chiclayo'),
('14', '02', 'Illino');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_categoria`
--

CREATE TABLE `tipo_categoria` (
  `id_tipo_categoria` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL
);

--
-- Dumping data for table `tipo_categoria`
--

INSERT INTO `tipo_categoria` (`id_tipo_categoria`, `nombre`, `id_categoria`) VALUES
(2, 'Dj', 12),
(3, 'Local - Campestre', 11),
(4, 'Orquesta', 12),
(5, 'Local Grande', 11);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` char(32) NOT NULL,
  `id_acceso` int(11) DEFAULT NULL,
  `estado` char(1) DEFAULT 'A'
);

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `email`, `pass`, `id_acceso`, `estado`) VALUES
(7, 'silviopd01@gmail.com', '202cb962ac59075b964b07152d234b70', 4, 'A'),
(11, 'pazvilcheznoemipaola@gmail.com', '202cb962ac59075b964b07152d234b70', 3, 'A'),
(12, 'simacain@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 'A'),
(13, 'final7@gmail.com', '202cb962ac59075b964b07152d234b70', 3, 'A'),
(14, 'autor6@gmail.com', '202cb962ac59075b964b07152d234b70', 4, 'A');
(15, 'admin@gmail.com', '202cb962ac59075b964b07152d234b70', 4, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acceso`
--
ALTER TABLE `acceso`
  ADD PRIMARY KEY (`id_acceso`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`email`),
  ADD KEY `fk_cliente_distrito` (`id_distrito`,`id_provincia`,`id_departamento`);

--
-- Indexes for table `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id_contacto`,`item`),
  ADD KEY `fk_contacto_cliente_0` (`email_cliente`),
  ADD KEY `fk_contacto_producto` (`id_producto`);

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id_departamento`);

--
-- Indexes for table `distrito`
--
ALTER TABLE `distrito`
  ADD PRIMARY KEY (`id_departamento`,`id_provincia`,`id_distrito`),
  ADD UNIQUE KEY `unq_distrito_id_distrito` (`id_distrito`,`id_provincia`,`id_departamento`),
  ADD KEY `fk_distrito_provincia` (`id_provincia`,`id_departamento`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`email`),
  ADD KEY `fk_empresa_distrito` (`id_distrito`,`id_provincia`,`id_departamento`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_producto`,`n_foto`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_producto_empresa` (`email`),
  ADD KEY `fk_producto_tipo_categoria` (`id_tipo_categoria`);

--
-- Indexes for table `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id_departamento`,`id_provincia`),
  ADD UNIQUE KEY `unq_provincia_id_provincia` (`id_provincia`,`id_departamento`),
  ADD KEY `fk_provincia_departamento` (`id_departamento`);

--
-- Indexes for table `tipo_categoria`
--
ALTER TABLE `tipo_categoria`
  ADD PRIMARY KEY (`id_tipo_categoria`),
  ADD KEY `fk_tipo_categoria_categoria` (`id_categoria`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `fk_usuario_acceso` (`id_acceso`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acceso`
--
ALTER TABLE `acceso`
  MODIFY `id_acceso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tipo_categoria`
--
ALTER TABLE `tipo_categoria`
  MODIFY `id_tipo_categoria` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_cliente_distrito` FOREIGN KEY (`id_distrito`,`id_provincia`,`id_departamento`) REFERENCES `distrito` (`id_distrito`, `id_provincia`, `id_departamento`);

--
-- Constraints for table `contacto`
--
ALTER TABLE `contacto`
  ADD CONSTRAINT `fk_contacto_cliente_0` FOREIGN KEY (`email_cliente`) REFERENCES `cliente` (`email`),
  ADD CONSTRAINT `fk_contacto_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `distrito`
--
ALTER TABLE `distrito`
  ADD CONSTRAINT `fk_distrito_provincia` FOREIGN KEY (`id_provincia`,`id_departamento`) REFERENCES `provincia` (`id_provincia`, `id_departamento`);

--
-- Constraints for table `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `fk_empresa_distrito` FOREIGN KEY (`id_distrito`,`id_provincia`,`id_departamento`) REFERENCES `distrito` (`id_distrito`, `id_provincia`, `id_departamento`);

--
-- Constraints for table `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `fk_foto_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_empresa` FOREIGN KEY (`email`) REFERENCES `empresa` (`email`),
  ADD CONSTRAINT `fk_producto_tipo_categoria` FOREIGN KEY (`id_tipo_categoria`) REFERENCES `tipo_categoria` (`id_tipo_categoria`);

--
-- Constraints for table `provincia`
--
ALTER TABLE `provincia`
  ADD CONSTRAINT `fk_provincia_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id_departamento`);

--
-- Constraints for table `tipo_categoria`
--
ALTER TABLE `tipo_categoria`
  ADD CONSTRAINT `fk_tipo_categoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_acceso` FOREIGN KEY (`id_acceso`) REFERENCES `acceso` (`id_acceso`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
