<?php
require_once("../conexion/conexion.php");

$sql = "SELECT * FROM acceso";
$result = $cnx->query($sql);

$combo = "<select class='form-control' id='modal_id_acceso'>";
$combo = $combo . "<option selected disabled> Seleccione una acceso </option>";

while ($reg = $result->fetchObject()) {
  $combo = $combo . "<option value='$reg->id_acceso'>$reg->nombre</option>";
}

$combo = $combo . "</select>";
echo $combo;
