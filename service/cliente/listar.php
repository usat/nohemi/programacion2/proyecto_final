<?php
require_once("../conexion/conexion.php");

$sql = "SELECT c.email, c.apellido_paterno, c.apellido_materno, c.nombres, c.telf1, c.telf2, c.direccion, c.fecha_registro, c.id_departamento, c.id_provincia, c.id_distrito, d.nombre as distrito, p.nombre as provincia, d1.nombre as departamento FROM cliente c INNER JOIN distrito d ON ( c.id_distrito = d.id_distrito AND c.id_provincia = d.id_provincia AND c.id_departamento = d.id_departamento ) INNER JOIN provincia p ON ( d.id_provincia = p.id_provincia AND d.id_departamento = p.id_departamento ) INNER JOIN departamento d1 ON ( p.id_departamento = d1.id_departamento ) ";
// $sql = "SELECT c.email, c.apellido_paterno, c.apellido_materno, c.nombres, c.telf1, c.telf2, c.sexo, c.direccion, c.fecha_nacimiento, c.fecha_registro, c.id_departamento, c.id_provincia, c.id_distrito, d.nombre as distrito, p.nombre as provincia, d1.nombre as departamento FROM cliente c INNER JOIN distrito d ON ( c.id_distrito = d.id_distrito AND c.id_provincia = d.id_provincia AND c.id_departamento = d.id_departamento ) INNER JOIN provincia p ON ( d.id_provincia = p.id_provincia AND d.id_departamento = p.id_departamento ) INNER JOIN departamento d1 ON ( p.id_departamento = d1.id_departamento ) ";

$rs = $cnx->query($sql);
$registro = $rs->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($registro);
