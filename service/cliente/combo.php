<?php
require_once("../conexion/conexion.php");

$sql = "SELECT * FROM cliente";
$result = $cnx->query($sql);

$combo = "<select class='form-control' id='modal_id_cliente'>";
$combo = $combo . "<option selected disabled> Seleccione una cliente </option>";

while ($reg = $result->fetchObject()) {
  $combo = $combo . "<option value='$reg->email'>$reg->nombres</option>";
}

$combo = $combo . "</select>";
echo $combo;
