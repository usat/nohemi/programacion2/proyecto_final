<?php
$driver = "mysql";
$base = "db_trabajo_final";
$host = "localhost";
$usuario = "root";
$contraseña = '';

$dsn = "$driver:dbname=$base;host=$host;charset=utf8";

try {
  $cnx = new PDO($dsn, $usuario, $contraseña);
} catch (PDOException $e) {
  echo 'Falló la conexión: ' . $e->getMessage();
}
