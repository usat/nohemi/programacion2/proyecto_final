<?php
require_once("../conexion/conexion.php");

$sql = "SELECT * FROM tipo_categoria";
$result = $cnx->query($sql);

$combo = "<select class='form-control' id='modal_id_tipo_categoria'>";
$combo = $combo . "<option selected disabled> Seleccione un tipo categoria </option>";

while ($reg = $result->fetchObject()) {
  $combo = $combo . "<option value='$reg->id_tipo_categoria'>$reg->nombre</option>";
}

$combo = $combo . "</select>";
echo $combo;
