<?php
require_once("../conexion/conexion.php");

$sql = "SELECT tc.id_tipo_categoria, tc.nombre as tipo_categoria, tc.id_categoria, c.nombre as categoria FROM tipo_categoria tc INNER JOIN categoria c ON ( tc.id_categoria = c.id_categoria )";

$rs = $cnx->query($sql);
$registro = $rs->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($registro);
