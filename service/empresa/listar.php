<?php
require_once("../conexion/conexion.php");

$sql = "SELECT e.email, e.nombre, e.rubro, e.direccion, e.telf1, e.telf2, e.id_departamento, e.id_provincia, e.id_distrito, e.fecha_registro, d.nombre as distrito, p.nombre as provincia, d1.nombre as departamento FROM empresa e INNER JOIN distrito d ON ( e.id_distrito = d.id_distrito AND e.id_provincia = d.id_provincia AND e.id_departamento = d.id_departamento ) INNER JOIN provincia p ON ( d.id_provincia = p.id_provincia AND d.id_departamento = p.id_departamento ) INNER JOIN departamento d1 ON ( p.id_departamento = d1.id_departamento )";

$rs = $cnx->query($sql);
$registro = $rs->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($registro);
