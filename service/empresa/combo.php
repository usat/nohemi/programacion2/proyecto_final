<?php
require_once("../conexion/conexion.php");

$sql = "SELECT * FROM empresa";
$result = $cnx->query($sql);

$combo = "<select class='form-control' id='modal_id_empresa'>";
$combo = $combo . "<option selected disabled> Seleccione una empresa </option>";

while ($reg = $result->fetchObject()) {
  $combo = $combo . "<option value='$reg->email'>$reg->nombre</option>";
}

$combo = $combo . "</select>";
echo $combo;
