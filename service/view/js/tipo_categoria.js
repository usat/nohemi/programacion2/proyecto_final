let tabla = document.getElementById("tabla")
let modal_title = document.getElementById("modal_titulo")
let accion = document.getElementById("accion").value

function listar() {
	$.ajax({
		type: "POST",
		url: "../tipo_categoria/listar.php",
		data: "",
		success: function(response) {
			let data = JSON.parse(response)
			llenar_tabla(data)
		}
	})
}

function llenar_tabla(data) {
	let html = ""

	html += `<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">`
	html += `<thead>`
	html += `<tr>`
	html += `<th>ID</th>`
	html += `<th>CATEGORIA</th>`
	html += `<th>TIPO CATEGORIA</th>`
	html += `<th class="text-center">ACCIONES</th>`
	html += `</tr>`
	html += `</thead>`
	html += `<tbody>`

	for (let i = 0; i < data.length; i++) {
		html += `<tr>`
		html += `<td>${data[i].id_tipo_categoria}</td>`
		html += `<td>${data[i].categoria}</td>`
		html += `<td>${data[i].tipo_categoria}</td>`
		html += `<td nowrap class="text-center">`
		html += `<button type="button" class="btn btn-warning" onclick="editar('${
			data[i].id_tipo_categoria
		}')" style="margin-right: 5px"><i class="flaticon-edit" style="padding-right: 0"></i></button>`
		html += `<button type="button" class="btn btn-danger" onclick="eliminar('${
			data[i].id_tipo_categoria
		}')" style="margin-right: 5px"><i class="flaticon-close" style="padding-right: 0"></i></button>`
		html += `</td>`
		html += `</tr>`
	}
	html += `</tbody>`
	html += `</table>`

	tabla.innerHTML = html

	var table = $("#kt_table_1")
	table.DataTable({
		responsive: true,
		paging: true
	})
}

function llenar_combo() {
	$.ajax({
		type: "POST",
		url: "../categoria/combo.php",
		data: "",
		success: function(response) {
			console.log(response)
			$("#modal_combo").html(response)
		}
	})
}

function eliminar(id_tipo_categoria) {
	console.log(id_tipo_categoria)

	Swal.fire({
		title: "¿Desea eliminar este registro?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Si, Eliminar"
	}).then(result => {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "../tipo_categoria/eliminar.php",
				data: { id_tipo_categoria: id_tipo_categoria },
				success: function(response) {
					if (response == 0) {
						Swal.fire("Elimado!", "El registro ha sido eliminado.", "success")

						listar()
					}
				}
			})
		}
	})
}

$("#formulario").submit(function(e) {
	e.preventDefault()

	let nombre = document.getElementById("modal_nombre").value
	let id_categoria = document.getElementById("modal_id_categoria").value

	if (accion == 0) {
		$.ajax({
			type: "POST",
			url: "../tipo_categoria/agregar.php",
			data: { id_categoria: id_categoria, nombre: nombre },
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Agregado!", "El registro ha sido agregado.", "success")

					listar()
				}
			}
		})
	} else {
		let id_tipo_categoria = document.getElementById("modal_id").value

		$.ajax({
			type: "POST",
			url: "../tipo_categoria/editar.php",
			data: {
				id_tipo_categoria: id_tipo_categoria,
				id_categoria: id_categoria,
				nombre: nombre
			},
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Editado!", "El registro ha sido editado.", "success")

					listar()
				}
			}
		})
	}
})

function editar(id_tipo_categoria) {
	$("#modal").modal("toggle")

	modal_title.innerHTML = "Editar"
	accion = 1

	$.ajax({
		type: "POST",
		url: "../tipo_categoria/leer.php",
		data: { id_tipo_categoria: id_tipo_categoria },
		success: function(response) {
			let data = JSON.parse(response)

			$("#modal_id").val(data.id_tipo_categoria)
			$("#modal_id_categoria").val(data.id_categoria)
			$("#modal_nombre").val(data.nombre)
		}
	})
}

function agregar() {
	accion = 0
	$("#modal_id").val("")
	$("#modal_nombre").val("")
	$("#modal_id_categoria").prop("selectedIndex", 0)
	modal_title.innerHTML = "Agregar"
}

listar()
llenar_combo()
