let tabla = document.getElementById("tabla")
let modal_title = document.getElementById("modal_titulo")
let accion = document.getElementById("accion").value

function listar() {
	$.ajax({
		type: "POST",
		url: "../cliente/listar.php",
		data: "",
		success: function(response) {
			let data = JSON.parse(response)
			llenar_tabla(data)
		}
	})
}

function llenar_tabla(data) {
	let html = ""

	html += `<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">`
	html += `<thead>`
	html += `<tr>`
	html += `<th>Email</th>`
	html += `<th>Apellido Pat</th>`
	html += `<th>Apellido Mat</th>`
	html += `<th>Nombres</th>`
	html += `<th>telf1</th>`
	html += `<th>telf2</th>`
	// html += `<th>Sexo</th>`
	html += `<th>Direccion</th>`
	// html += `<th>Fecha Nac</th>`
	html += `<th>Departamento</th>`
	html += `<th>Provincia</th>`
	html += `<th>Distrito</th>`
	html += `<th class="text-center">ACCIONES</th>`
	html += `</tr>`
	html += `</thead>`
	html += `<tbody>`

	for (let i = 0; i < data.length; i++) {
		html += `<tr>`
		html += `<td>${data[i].email}</td>`
		html += `<td>${data[i].apellido_paterno}</td>`
		html += `<td>${data[i].apellido_materno}</td>`
		html += `<td>${data[i].nombres}</td>`
		html += `<td>${data[i].telf1}</td>`
		html += `<td>${data[i].telf2}</td>`
		// html += `<td>${data[i].sexo}</td>`
		html += `<td>${data[i].direccion}</td>`
		// html += `<td>${data[i].fecha_nacimiento}</td>`
		html += `<td>${data[i].departamento}</td>`
		html += `<td>${data[i].provincia}</td>`
		html += `<td>${data[i].distrito}</td>`
		html += `<td nowrap class="text-center">`
		html += `<button type="button" class="btn btn-warning" onclick="editar('${
			data[i].email
		}')" style="margin-right: 5px"><i class="flaticon-edit" style="padding-right: 0"></i></button>`
		html += `<button type="button" class="btn btn-danger" onclick="eliminar('${
			data[i].email
		}')" style="margin-right: 5px"><i class="flaticon-close" style="padding-right: 0"></i></button>`
		html += `</td>`
		html += `</tr>`
	}
	html += `</tbody>`
	html += `</table>`

	tabla.innerHTML = html

	var table = $("#kt_table_1")
	table.DataTable({
		responsive: true,
		paging: true
	})
}

function llenar_combo_departamento() {
	$.ajax({
		type: "POST",
		url: "../departamento/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			$("#modal_combo_departamento").html(response)
		}
	})
}

function llenar_combo_provincia(id_departamento) {
	$.ajax({
		type: "POST",
		url: "../provincia/combo.php",
		data: { id_departamento: id_departamento },
		success: function(response) {
			// console.log(response)
			$("#modal_combo_provincia").html(response)
		}
	})
}

function llenar_combo_distrito(id_departamento, id_provincia) {
	$.ajax({
		type: "POST",
		url: "../distrito/combo.php",
		data: { id_departamento: id_departamento, id_provincia: id_provincia },
		success: function(response) {
			// console.log(response)
			$("#modal_combo_distrito").html(response)
		}
	})
}

$(document).on("change", "#modal_id_departamento", function() {
	let id_departamento = $("#modal_id_departamento").val()

	$("#modal_combo_provincia").html("")
	$("#modal_combo_distrito").html("")

	llenar_combo_provincia(id_departamento)
})

$(document).on("change", "#modal_id_provincia", function() {
	let id_departamento = $("#modal_id_departamento").val()
	let id_provincia = $("#modal_id_provincia").val()

	$("#modal_combo_distrito").html("")

	llenar_combo_distrito(id_departamento, id_provincia)
})

function eliminar(email) {
	Swal.fire({
		title: "¿Desea eliminar este registro?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Si, Eliminar"
	}).then(result => {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "../cliente/eliminar.php",
				data: { email: email },
				success: function(response) {
					if (response == 0) {
						Swal.fire("Elimado!", "El registro ha sido eliminado.", "success")

						listar()
					}
				}
			})
		}
	})
}

$("#formulario").submit(function(e) {
	e.preventDefault()

	let email = document.getElementById("modal_email").value
	let apellido_paterno = document.getElementById("modal_apellido_paterno").value
	let apellido_materno = document.getElementById("modal_apellido_materno").value
	let nombres = document.getElementById("modal_nombre").value
	// let sexo = document.getElementById("modal_sexo").value
	let direccion = document.getElementById("modal_direccion").value
	// let fecha_nacimiento = document.getElementById("modal_fecha_nacimiento").value
	let telf1 = document.getElementById("modal_telf1").value
	let telf2 = document.getElementById("modal_telf2").value
	let id_departamento = document.getElementById("modal_id_departamento").value
	let id_provincia = document.getElementById("modal_id_provincia").value
	let id_distrito = document.getElementById("modal_id_distrito").value

	if (accion == 0) {
		$.ajax({
			type: "POST",
			url: "../cliente/agregar.php",
			data: {
				email: email,
				apellido_paterno: apellido_paterno,
				apellido_materno: apellido_materno,
				nombres: nombres,
				// sexo: sexo,
				direccion: direccion,
				// fecha_nacimiento: fecha_nacimiento
				// 	.split("/")
				// 	.reverse()
				// 	.join("-"),
				telf1: telf1,
				telf2: telf2,
				id_departamento: id_departamento,
				id_provincia: id_provincia,
				id_distrito: id_distrito
			},
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Agregado!", "El registro ha sido agregado.", "success")

					listar()
				}
			}
		})
	} else {
		$.ajax({
			type: "POST",
			url: "../cliente/editar.php",
			data: {
				email: email,
				apellido_paterno: apellido_paterno,
				apellido_materno: apellido_materno,
				nombres: nombres,
				// sexo: sexo,
				direccion: direccion,
				// fecha_nacimiento: fecha_nacimiento
				// 	.split("/")
				// 	.reverse()
				// 	.join("-"),
				telf1: telf1,
				telf2: telf2,
				id_departamento: id_departamento,
				id_provincia: id_provincia,
				id_distrito: id_distrito
			},
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Editado!", "El registro ha sido editado.", "success")

					listar()
				}
			}
		})
	}
})

function editar(email) {
	$("#modal").modal("toggle")

	modal_title.innerHTML = "Editar"
	accion = 1

	$.ajax({
		type: "POST",
		url: "../cliente/leer.php",
		data: { email: email },
		success: function(response) {
			let data = JSON.parse(response)
			console.log(data.id_provincia)

			$("#modal_email").val(data.email)
			$("#modal_apellido_paterno").val(data.apellido_paterno)
			$("#modal_apellido_materno").val(data.apellido_materno)
			$("#modal_nombre").val(data.nombres)
			// $("#modal_sexo").val(data.sexo)
			$("#modal_direccion").val(data.direccion)
			// $("#modal_fecha_nacimiento").val(data.fecha_nacimiento)
			$("#modal_telf1").val(data.telf1)
			$("#modal_telf1").val(data.telf1)
			$("#modal_telf2").val(data.telf2)

			$("#modal_id_departamento").val(data.id_departamento)
			$("#modal_id_provincia").val(data.id_provincia)
		}
	})
}

function agregar() {
	accion = 0
	$("#modal_email").val("")
	$("#modal_nombre").val("")
	$("#modal_id_departamento").prop("selectedIndex", 0)
	$("#modal_combo_provincia").html("")
	modal_title.innerHTML = "Agregar"
}

listar()
llenar_combo_departamento()
