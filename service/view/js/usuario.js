let tabla = document.getElementById("tabla")
let modal_title = document.getElementById("modal_titulo")
let accion = document.getElementById("accion").value

function listar() {
	$.ajax({
		type: "POST",
		url: "../usuario/listar.php",
		data: "",
		success: function(response) {
			let data = JSON.parse(response)
			llenar_tabla(data)
		}
	})
}

function llenar_tabla(data) {
	let html = ""

	html += `<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">`
	html += `<thead>`
	html += `<tr>`
	html += `<th>Email</th>`
	html += `<th>Acceso</th>`
	html += `<th>Estado</th>`
	html += `<th class="text-center">ACCIONES</th>`
	html += `</tr>`
	html += `</thead>`
	html += `<tbody>`

	for (let i = 0; i < data.length; i++) {
		html += `<tr>`
		html += `<td>${data[i].email}</td>`
		html += `<td>${data[i].acceso}</td>`
		html += `<td>${data[i].estado}</td>`
		html += `<td nowrap class="text-center">`
		html += `<button type="button" class="btn btn-warning" onclick="editar('${
			data[i].id_usuario
		}')" style="margin-right: 5px"><i class="flaticon-edit" style="padding-right: 0"></i></button>`
		html += `<button type="button" class="btn btn-danger" onclick="eliminar('${
			data[i].id_usuario
		}')" style="margin-right: 5px"><i class="flaticon-close" style="padding-right: 0"></i></button>`
		html += `</td>`
		html += `</tr>`
	}
	html += `</tbody>`
	html += `</table>`

	tabla.innerHTML = html

	var table = $("#kt_table_1")
	table.DataTable({
		responsive: true,
		paging: true
	})
}

function llenar_combo_acceso() {
	$.ajax({
		type: "POST",
		url: "../acceso/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			$("#modal_combo_acceso").html(response)
		}
	})
}

function llenar_combo_empresa() {
	$.ajax({
		type: "POST",
		url: "../empresa/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			$("#modal_combo_cliente_empresa").html(response)
		}
	})
}

function llenar_combo_cliente() {
	$.ajax({
		type: "POST",
		url: "../cliente/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			$("#modal_combo_cliente_empresa").html(response)
		}
	})
}

$("input[type=radio][name=radio_cliente_empresa]").change(function() {
	if (this.value == "cliente") {
		llenar_combo_cliente()
	} else if (this.value == "empresa") {
		llenar_combo_empresa()
	}
})

function eliminar(id_usuario) {
	Swal.fire({
		title: "¿Desea eliminar este registro?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Si, Eliminar"
	}).then(result => {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "../usuario/eliminar.php",
				data: { id_usuario: id_usuario },
				success: function(response) {
					if (response == 0) {
						Swal.fire("Elimado!", "El registro ha sido eliminado.", "success")

						listar()
					}
				}
			})
		}
	})
}

$("#formulario").submit(function(e) {
	e.preventDefault()

	let radio = $("input[name=radio_cliente_empresa]:checked").val()
	let email = ""
	if (radio == "cliente") {
		email = document.getElementById("modal_id_cliente").value
	} else {
		email = document.getElementById("modal_id_empresa").value
	}

	let id_acceso = document.getElementById("modal_id_acceso").value
	let password = document.getElementById("modal_password").value
	let estado = document.getElementById("modal_estado").value

	if (password.length == 0) {
		password = 0
	}

	console.log(radio, email, id_acceso, password, estado)
	// return

	if (accion == 0) {
		$.ajax({
			type: "POST",
			url: "../usuario/agregar.php",
			data: { email, password, id_acceso },
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Agregado!", "El registro ha sido agregado.", "success")

					listar()
				}
			}
		})
	} else {
		let id_usuario = document.getElementById("modal_id").value

		$.ajax({
			type: "POST",
			url: "../usuario/editar.php",
			data: { id_usuario, email, password, id_acceso },
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Editado!", "El registro ha sido editado.", "success")

					listar()
				}
			}
		})
	}
})

function editar(id_usuario) {
	$("#modal").modal("toggle")

	modal_title.innerHTML = "Editar"
	accion = 1

	$.ajax({
		type: "POST",
		url: "../usuario/leer.php",
		data: { id_usuario },
		success: function(response) {
			let data = JSON.parse(response)
			console.log(data.id_provincia)

			$("#modal_id").val(data.id_usuario)

			if (data.acceso == "cliente") {
				$("input:radio[name=radio_cliente_empresa]")[0].checked = true
				// $('input[type=radio][name=radio_cliente_empresa]').change()
				$("#modal_id_cliente").val(data.email)
			} else {
				$("input:radio[name=radio_cliente_empresa]")[1].checked = true

				$("input[type=radio][name=radio_cliente_empresa]").change()
				$(document).on("show.bs.modal", "#myModal", function() {
					$("#modal_id_empresa").val(data.email)
				})
			}

			$("#modal_id_acceso").val(data.id_acceso)
			$("#modal_estado").val(data.estado)
		}
	})
}

function agregar() {
	accion = 0
	$("#modal_id").val("")
	$("#modal_nombre").val("")
	$("#modal_id_departamento").prop("selectedIndex", 0)
	$("#modal_combo_provincia").html("")
	modal_title.innerHTML = "Agregar"
}

listar()
llenar_combo_cliente()
llenar_combo_acceso()
