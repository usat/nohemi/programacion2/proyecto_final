document.getElementById("kt_login_signin_submit").onclick = () => {
	// alert("hola")
	// debugger
	let email = document.getElementById("txtemail").value
	let password = document.getElementById("txtpassword").value

	$.ajax({
		type: "POST",
		url: "../admin/login.php",
		data: { email, password },
		success: function(response) {
			console.log(response)
			if (response) {
				console.log(response)
				// debugger
				let data = JSON.parse(response)
				localStorage.setItem("id_admin", data.id_usuario)
				localStorage.setItem("email_admin", data.email)
				localStorage.setItem("id_acceso_admin", data.id_acceso)
				localStorage.setItem("acceso_admin", data.acceso)

				let correo = localStorage.getItem("email")
				if (!correo || correo === "undefined") {
					alert("datos incorrectos")
					location.reload()
				} else {
					window.location = `producto.html`
				}
			}
		}
	})
}
