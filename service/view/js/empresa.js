let tabla = document.getElementById("tabla")
let modal_title = document.getElementById("modal_titulo")
let accion = document.getElementById("accion").value

function listar() {
	$.ajax({
		type: "POST",
		url: "../empresa/listar.php",
		data: "",
		success: function(response) {
			let data = JSON.parse(response)
			llenar_tabla(data)
		}
	})
}

function llenar_tabla(data) {
	let html = ""

	html += `<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">`
	html += `<thead>`
	html += `<tr>`
	html += `<th>Email</th>`
	html += `<th>Nombre</th>`
	html += `<th>Rubro</th>`
	html += `<th>Direccion</th>`
	html += `<th>telf1</th>`
	html += `<th>telf2</th>`
	html += `<th>Departamento</th>`
	html += `<th>Provincia</th>`
	html += `<th>Distrito</th>`
	html += `<th class="text-center">ACCIONES</th>`
	html += `</tr>`
	html += `</thead>`
	html += `<tbody>`

	for (let i = 0; i < data.length; i++) {
		html += `<tr>`
		html += `<td>${data[i].email}</td>`
		html += `<td>${data[i].nombre}</td>`
		html += `<td>${data[i].rubro}</td>`
		html += `<td>${data[i].direccion}</td>`
		html += `<td>${data[i].telf1}</td>`
		html += `<td>${data[i].telf2}</td>`
		html += `<td>${data[i].departamento}</td>`
		html += `<td>${data[i].provincia}</td>`
		html += `<td>${data[i].distrito}</td>`
		html += `<td nowrap class="text-center">`
		html += `<button type="button" class="btn btn-warning" onclick="editar('${
			data[i].email
		}')" style="margin-right: 5px"><i class="flaticon-edit" style="padding-right: 0"></i></button>`
		html += `<button type="button" class="btn btn-danger" onclick="eliminar('${
			data[i].email
		}')" style="margin-right: 5px"><i class="flaticon-close" style="padding-right: 0"></i></button>`
		html += `</td>`
		html += `</tr>`
	}
	html += `</tbody>`
	html += `</table>`

	tabla.innerHTML = html

	var table = $("#kt_table_1")
	table.DataTable({
		responsive: true,
		paging: true
	})
}

function llenar_combo_departamento() {
	$.ajax({
		type: "POST",
		url: "../departamento/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			$("#modal_combo_departamento").html(response)
		}
	})
}

function llenar_combo_provincia(id_departamento) {
	$.ajax({
		type: "POST",
		url: "../provincia/combo.php",
		data: { id_departamento: id_departamento },
		success: function(response) {
			// console.log(response)
			$("#modal_combo_provincia").html(response)
		}
	})
}

function llenar_combo_distrito(id_departamento, id_provincia) {
	$.ajax({
		type: "POST",
		url: "../distrito/combo.php",
		data: { id_departamento: id_departamento, id_provincia: id_provincia },
		success: function(response) {
			// console.log(response)
			$("#modal_combo_distrito").html(response)
		}
	})
}

$(document).on("change", "#modal_id_departamento", function() {
	let id_departamento = $("#modal_id_departamento").val()

	$("#modal_combo_provincia").html("")
	$("#modal_combo_distrito").html("")

	llenar_combo_provincia(id_departamento)
})

$(document).on("change", "#modal_id_provincia", function() {
	let id_departamento = $("#modal_id_departamento").val()
	let id_provincia = $("#modal_id_provincia").val()

	$("#modal_combo_distrito").html("")

	llenar_combo_distrito(id_departamento, id_provincia)
})

function eliminar(email) {
	Swal.fire({
		title: "¿Desea eliminar este registro?",
		text:
			"si deses eliminar este registro, antes debes eliminar todos los productos de esta empresa",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Si, Eliminar"
	}).then(result => {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "../empresa/eliminar.php",
				data: { email: email },
				success: function(response) {
					if (response == 0) {
						Swal.fire("Elimado!", "El registro ha sido eliminado.", "success")

						listar()
					}
				}
			})
		}
	})
}

$("#formulario").submit(function(e) {
	e.preventDefault()

	let email = document.getElementById("modal_email").value
	let nombre = document.getElementById("modal_nombre").value
	let rubro = document.getElementById("modal_rubro").value
	let direccion = document.getElementById("modal_direccion").value
	let telf1 = document.getElementById("modal_telf1").value
	let telf2 = document.getElementById("modal_telf2").value
	let id_departamento = document.getElementById("modal_id_departamento").value
	let id_provincia = document.getElementById("modal_id_provincia").value
	let id_distrito = document.getElementById("modal_id_distrito").value

	if (accion == 0) {
		$.ajax({
			type: "POST",
			url: "../empresa/agregar.php",
			data: {
				email: email,
				nombre: nombre,
				rubro: rubro,
				direccion: direccion,
				telf1: telf1,
				telf2: telf2,
				id_distrito: id_distrito,
				id_departamento: id_departamento,
				id_provincia: id_provincia
			},
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Agregado!", "El registro ha sido agregado.", "success")

					listar()
				}
			}
		})
	} else {
		$.ajax({
			type: "POST",
			url: "../empresa/editar.php",
			data: {
				email: email,
				nombre: nombre,
				rubro: rubro,
				direccion: direccion,
				telf1: telf1,
				telf2: telf2,
				id_distrito: id_distrito,
				id_departamento: id_departamento,
				id_provincia: id_provincia
			},
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Editado!", "El registro ha sido editado.", "success")

					listar()
				}
			}
		})
	}
})

function editar(email) {
	$("#modal").modal("toggle")

	modal_title.innerHTML = "Editar"
	accion = 1

	$.ajax({
		type: "POST",
		url: "../empresa/leer.php",
		data: { email: email },
		success: function(response) {
			let data = JSON.parse(response)

			$("#modal_email").val(data.email)
			$("#modal_nombre").val(data.nombre)
			$("#modal_rubro").val(data.rubro)
			$("#modal_direccion").val(data.direccion)
			$("#modal_telf1").val(data.telf1)
			$("#modal_telf2").val(data.telf2)

			$("#modal_id_departamento").val(data.id_departamento)
			llenar_combo_provincia(data.id_departamento)
			$(document).on("show.bs.modal", "#myModal", function() {
				debugger
				$("#modal_id_provincia").val(data.id_provincia)
			})

			llenar_combo_distrito(data.id_departamento, data.id_provincia)
			$("#modal_id_distrito").val(data.id_distrito)
		}
	})
}

function agregar() {
	accion = 0
	$("#modal_id").val("")
	$("#modal_nombre").val("")
	$("#modal_id_departamento").prop("selectedIndex", 0)
	$("#modal_combo_provincia").html("")
	modal_title.innerHTML = "Agregar"
}

listar()
llenar_combo_departamento()
