let tabla = document.getElementById("tabla")
let modal_title = document.getElementById("modal_titulo")
let accion = document.getElementById("accion").value

function listar() {
	$.ajax({
		type: "POST",
		url: "../distrito/listar.php",
		data: "",
		success: function(response) {
			let data = JSON.parse(response)
			llenar_tabla(data)
		}
	})
}

function llenar_tabla(data) {
	let html = ""

	html += `<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">`
	html += `<thead>`
	html += `<tr>`
	html += `<th>UBIGEO</th>`
	html += `<th>DEPARTAMENTO</th>`
	html += `<th>PROVINCIA</th>`
	html += `<th>DISTRITO</th>`
	html += `<th class="text-center">ACCIONES</th>`
	html += `</tr>`
	html += `</thead>`
	html += `<tbody>`

	for (let i = 0; i < data.length; i++) {
		html += `<tr>`
		html += `<td>${data[i].id_departamento}${data[i].id_provincia}${
			data[i].id_distrito
		}</td>`
		html += `<td>${data[i].departamento}</td>`
		html += `<td>${data[i].provincia}</td>`
		html += `<td>${data[i].distrito}</td>`
		html += `<td nowrap class="text-center">`
		html += `<button type="button" class="btn btn-warning" onclick="editar('${
			data[i].id_departamento
		}','${data[i].id_provincia}','${
			data[i].id_distrito
		}')" style="margin-right: 5px"><i class="flaticon-edit" style="padding-right: 0"></i></button>`
		html += `<button type="button" class="btn btn-danger" onclick="eliminar('${
			data[i].id_departamento
		}','${data[i].id_provincia}','${
			data[i].id_distrito
		}')" style="margin-right: 5px"><i class="flaticon-close" style="padding-right: 0"></i></button>`
		html += `</td>`
		html += `</tr>`
	}
	html += `</tbody>`
	html += `</table>`

	tabla.innerHTML = html

	var table = $("#kt_table_1")
	table.DataTable({
		responsive: true,
		paging: true
	})
}

function llenar_combo_departamento() {
	$.ajax({
		type: "POST",
		url: "../departamento/combo.php",
		data: "",
		success: function(response) {
			// console.log(response)
			$("#modal_combo_departamento").html(response)
		}
	})
}

function llenar_combo_provincia(id_departamento) {
	$.ajax({
		type: "POST",
		url: "../provincia/combo.php",
		data: { id_departamento: id_departamento },
		success: function(response) {
			// console.log(response)
			$("#modal_combo_provincia").html(response)
		}
	})
}

$(document).on("change", "#modal_id_departamento", function() {
	let id_departamento = $("#modal_id_departamento").val()

	$("#modal_id_provincia").html("")

	llenar_combo_provincia(id_departamento)
})

function eliminar(id_departamento, id_provincia, id_distrito) {
	Swal.fire({
		title: "¿Desea eliminar este registro?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#3085d6",
		cancelButtonColor: "#d33",
		confirmButtonText: "Si, Eliminar"
	}).then(result => {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "../distrito/eliminar.php",
				data: {
					id_departamento: id_departamento,
					id_provincia: id_provincia,
					id_distrito: id_distrito
				},
				success: function(response) {
					if (response == 0) {
						Swal.fire("Elimado!", "El registro ha sido eliminado.", "success")

						listar()
					}
				}
			})
		}
	})
}

$("#formulario").submit(function(e) {
	e.preventDefault()

	let nombre = document.getElementById("modal_nombre").value
	let id_departamento = document.getElementById("modal_id_departamento").value
	let id_provincia = document.getElementById("modal_id_provincia").value
	let id_distrito = document.getElementById("modal_id").value

	if (accion == 0) {
		$.ajax({
			type: "POST",
			url: "../distrito/agregar.php",
			data: {
				id_distrito: id_distrito,
				id_departamento: id_departamento,
				id_provincia: id_provincia,
				nombre: nombre
			},
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Agregado!", "El registro ha sido agregado.", "success")

					listar()
				}
			}
		})
	} else {
		$.ajax({
			type: "POST",
			url: "../distrito/editar.php",
			data: {
				id_distrito: id_distrito,
				id_departamento: id_departamento,
				id_provincia: id_provincia,
				nombre: nombre
			},
			success: function(response) {
				if (response == 0) {
					$("#modal").modal("toggle")

					Swal.fire("Editado!", "El registro ha sido editado.", "success")

					listar()
				}
			}
		})
	}
})

function editar(id_departamento, id_provincia, id_distrito) {
	$("#modal").modal("toggle")

	modal_title.innerHTML = "Editar"
	accion = 1

	llenar_combo_provincia(id_departamento)

	$.ajax({
		type: "POST",
		url: "../distrito/leer.php",
		data: {
			id_departamento: id_departamento,
			id_provincia: id_provincia,
			id_distrito: id_distrito
		},
		success: function(response) {
			let data = JSON.parse(response)
			console.log(data.id_provincia)

			$("#modal_id").val(data.id_distrito)

			$("#modal_id_departamento").val(data.id_departamento)
			$("#modal_id_provincia").val(data.id_provincia)

			$("#modal_nombre").val(data.nombre)
		}
	})
}

function agregar() {
	accion = 0
	$("#modal_id").val("")
	$("#modal_nombre").val("")
	$("#modal_id_departamento").prop("selectedIndex", 0)
	$("#modal_combo_provincia").html("")
	modal_title.innerHTML = "Agregar"
}

listar()
llenar_combo_departamento()
