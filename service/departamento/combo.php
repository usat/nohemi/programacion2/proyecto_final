<?php
require_once("../conexion/conexion.php");

$sql = "SELECT * FROM departamento";
$result = $cnx->query($sql);

$combo = "<select class='form-control' id='modal_id_departamento'>";
$combo = $combo . "<option selected disabled> Seleccione una departamento </option>";

while ($reg = $result->fetchObject()) {
  $combo = $combo . "<option value='$reg->id_departamento'>$reg->nombre</option>";
}

$combo = $combo . "</select>";
echo $combo;
