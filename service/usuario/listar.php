<?php
require_once("../conexion/conexion.php");

$sql = "SELECT u.*, (SELECT a.nombre FROM acceso a WHERE a.id_acceso=u.id_acceso) AS acceso FROM usuario u";

$rs = $cnx->query($sql);
$registro = $rs->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($registro);
