<?php
require_once("../conexion/conexion.php");

$sql = "SELECT * FROM categoria";
$result = $cnx->query($sql);

$combo = "<select class='form-control' id='modal_id_categoria'>";
$combo = $combo . "<option selected disabled> Seleccione una categoria </option>";

while ($reg = $result->fetchObject()) {
  $combo = $combo . "<option value='$reg->id_categoria'>$reg->nombre</option>";
}

$combo = $combo . "</select>";
echo $combo;
