<?php
require_once("../conexion/conexion.php");

$id_producto = $_POST['id_producto'];

$sql = "SELECT p.id_producto,p.email, p.id_tipo_categoria, p.nombre, e.nombre as empresa, tc.nombre as tipo_categoria, c.nombre as categoria, f.foto, p.descripcion FROM producto p INNER JOIN empresa e ON ( p.email = e.email ) INNER JOIN tipo_categoria tc ON ( p.id_tipo_categoria = tc.id_tipo_categoria ) INNER JOIN categoria c ON ( tc.id_categoria = c.id_categoria ) INNER JOIN foto f on (f.id_producto=p.id_producto) WHERE p.id_producto=$id_producto";

$rs = $cnx->query($sql);
$registro = $rs->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($registro);
