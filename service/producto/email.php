<?php
require_once("../conexion/conexion.php");

$id_producto = $_POST['id_producto'];

$sql = "SELECT p.email, p.id_tipo_categoria, p.nombre, p.id_producto, p.descripcion, tc.nombre as tipo_categoria, c.nombre as categoria, e.nombre as empresa, e.rubro, e.direccion, e.telf1, e.telf2, d.nombre as departamento, p1.nombre as provincia, d1.nombre as distrito, (SELECT foto from foto LIMIT 1) as foto FROM db_trabajo_final.producto p INNER JOIN db_trabajo_final.tipo_categoria tc ON ( p.id_tipo_categoria = tc.id_tipo_categoria ) INNER JOIN db_trabajo_final.categoria c ON ( tc.id_categoria = c.id_categoria ) INNER JOIN db_trabajo_final.empresa e ON ( p.email = e.email ) INNER JOIN db_trabajo_final.distrito d ON ( e.id_distrito = d.id_distrito AND e.id_provincia = d.id_provincia AND e.id_departamento = d.id_departamento ) INNER JOIN db_trabajo_final.provincia p1 ON ( d.id_provincia = p1.id_provincia AND d.id_departamento = p1.id_departamento ) INNER JOIN db_trabajo_final.departamento d1 ON ( p1.id_departamento = d1.id_departamento ) WHERE p.id_producto=$id_producto ORDER BY e.nombre ASC";

$rs = $cnx->query($sql);
$registro = $rs->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($registro);
