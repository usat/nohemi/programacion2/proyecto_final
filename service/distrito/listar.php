<?php
require_once("../conexion/conexion.php");

$sql = "SELECT d.id_departamento, d.id_provincia, d.id_distrito, d.nombre as distrito, p.nombre as provincia, d1.nombre as departamento FROM distrito d INNER JOIN provincia p ON ( d.id_provincia = p.id_provincia AND d.id_departamento = p.id_departamento  ) INNER JOIN departamento d1 ON ( p.id_departamento = d1.id_departamento  ) ";

$rs = $cnx->query($sql);
$registro = $rs->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($registro);
